package com.xing.service;

import com.xing.model.User;

public interface UserService {
    User selectUserByName();
}

